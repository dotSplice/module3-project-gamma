from queries.trips import TripsQueries
from main import app
from fastapi.testclient import TestClient
from models import TripIn, TripOut, AccountOut
from authenticator import authenticator


client = TestClient(app)


def fake_get_current_account_data():
    return {
        "id": "123",
        "username": "fake-user",
        "email": "fake@email.com",
    }


class FakeTripsQueries:
    def get_all_trips(self):
        return [
            {
                "id": "fake-id-from-db",
                "name": "test",
                "type_of_trip": "string",
                "max_attendees": "string",
                "from_date": "string",
                "to_date": "string",
                "location": {
                    "country": "string",
                    "state": "string",
                    "city": "string",
                },
                "description": "string",
                "image": "string",
                "account": {
                    "id": "123",
                    "username": "fake-user",
                    "email": "fake@email.com",
                },
                "attendees": [
                    {"id": "string", "username": "string", "email": "string"}
                ],
            }
        ]

    def create(self, trip_in: TripIn, account: AccountOut):
        trip = trip_in.dict()

        trip["account"] = account
        trip["id"] = "fake-id-from-db"

        return TripOut(**trip)

    def update(self, id: str, trip: TripOut, account: AccountOut):
        trip = trip.dict()

        trip["id"] = id
        trip["account"]["id"] = account["id"]
        return TripOut(**trip)

    def get_my_trips(self, account_id: str):
        return [
            {
                "id": "fake-id-from-db",
                "name": "test",
                "type_of_trip": "string",
                "max_attendees": "string",
                "from_date": "string",
                "to_date": "string",
                "location": {
                    "country": "string",
                    "state": "string",
                    "city": "string",
                },
                "description": "string",
                "image": "string",
                "account": {
                    "id": "123",
                    "username": "fake-user",
                    "email": "fake@email.com",
                },
                "attendees": [
                    {"id": "string", "username": "string", "email": "string"}
                ],
            }
        ]

    def delete(self, id: str, account_id: str):
        return True

    def view_trip(self, id: str):
        return {
            "name": "Test Name",
            "type_of_trip": "Test Type",
            "max_attendees": "1",
            "from_date": "test",
            "to_date": "test",
            "location": {
                "country": "Australia",
                "state": "South Australia",
                "city": "Morphett Vale",
            },
            "description": "Test",
            "image": "string",
            "attendees": [
                {
                    "id": "string",
                    "username": "string",
                    "email": "string@gmail.com",
                }
            ],
            "account": {
                "id": "123",
                "username": "fake-user",
                "email": "fake@email.com",
            },
            "id": "fake-id-from-db",
        }


def test_get_all_trips():
    app.dependency_overrides[TripsQueries] = FakeTripsQueries
    res = client.get("/api/trips")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "trips": [
            {
                "id": "fake-id-from-db",
                "name": "test",
                "type_of_trip": "string",
                "max_attendees": "string",
                "from_date": "string",
                "to_date": "string",
                "location": {
                    "country": "string",
                    "state": "string",
                    "city": "string",
                },
                "description": "string",
                "image": "string",
                "account": {
                    "id": "123",
                    "username": "fake-user",
                    "email": "fake@email.com",
                },
                "attendees": [
                    {"id": "string", "username": "string", "email": "string"}
                ],
            }
        ]
    }


def test_create():
    app.dependency_overrides[TripsQueries] = FakeTripsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    trip_in = {
        "name": "test",
        "type_of_trip": "string",
        "max_attendees": "string",
        "from_date": "string",
        "to_date": "string",
        "location": {
            "country": "string",
            "state": "string",
            "city": "string",
        },
        "description": "string",
        "image": "string",
        "attendees": [
            {
                "id": "string",
                "username": "string",
                "email": "string",
            }
        ],
    }

    res = client.post("/api/trips", json=trip_in)
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "id": "fake-id-from-db",
        "name": "test",
        "type_of_trip": "string",
        "max_attendees": "string",
        "from_date": "string",
        "to_date": "string",
        "location": {
            "country": "string",
            "state": "string",
            "city": "string",
        },
        "description": "string",
        "image": "string",
        "account": {
            "id": "123",
            "username": "fake-user",
            "email": "fake@email.com",
        },
        "attendees": [
            {
                "id": "string",
                "username": "string",
                "email": "string",
            }
        ],
    }


def test_update():
    app.dependency_overrides[TripsQueries] = FakeTripsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    trip = {
        "id": "fake-id-from-db",
        "name": "updated test",
        "type_of_trip": "string",
        "max_attendees": "string",
        "from_date": "string",
        "to_date": "string",
        "location": {
            "country": "string",
            "state": "string",
            "city": "string",
        },
        "description": "string",
        "image": "string",
        "account": {
            "id": "123",
            "username": "fake-user",
            "email": "fake@email.com",
        },
        "attendees": [
            {
                "id": "string",
                "username": "string",
                "email": "string",
            }
        ],
    }

    res = client.put(f"/api/trips/{trip['id']}", json=trip)
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "id": "fake-id-from-db",
        "name": "updated test",
        "type_of_trip": "string",
        "max_attendees": "string",
        "from_date": "string",
        "to_date": "string",
        "location": {
            "country": "string",
            "state": "string",
            "city": "string",
        },
        "description": "string",
        "image": "string",
        "account": {
            "id": "123",
            "username": "fake-user",
            "email": "fake@email.com",
        },
        "attendees": [
            {
                "id": "string",
                "username": "string",
                "email": "string",
            }
        ],
    }


def test_get_my_trips():
    app.dependency_overrides[TripsQueries] = FakeTripsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/api/trips/mine")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "trips": [
            {
                "id": "fake-id-from-db",
                "name": "test",
                "type_of_trip": "string",
                "max_attendees": "string",
                "from_date": "string",
                "to_date": "string",
                "location": {
                    "country": "string",
                    "state": "string",
                    "city": "string",
                },
                "description": "string",
                "image": "string",
                "account": {
                    "id": "123",
                    "username": "fake-user",
                    "email": "fake@email.com",
                },
                "attendees": [
                    {"id": "string", "username": "string", "email": "string"}
                ],
            }
        ]
    }


def test_delete_trip():
    app.dependency_overrides[TripsQueries] = FakeTripsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/api/trips/mine/64b581a3cfec1dc3ecc6e4bc")
    data = res.json()

    assert res.status_code == 200
    assert {"trips": True} == data


def test_view_trip():
    app.dependency_overrides[TripsQueries] = FakeTripsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    res = client.get("/api/trips/view/{id}")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "name": "Test Name",
        "type_of_trip": "Test Type",
        "max_attendees": "1",
        "from_date": "test",
        "to_date": "test",
        "location": {
            "country": "Australia",
            "state": "South Australia",
            "city": "Morphett Vale",
        },
        "description": "Test",
        "image": "string",
        "attendees": [
            {
                "id": "string",
                "username": "string",
                "email": "string@gmail.com",
            }
        ],
        "account": {
            "id": "123",
            "username": "fake-user",
            "email": "fake@email.com",
        },
        "id": "fake-id-from-db",
    }
