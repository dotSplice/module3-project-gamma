from pydantic import BaseModel
from typing import List, Optional
from jwtdown_fastapi.authentication import Token


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    email: str
    password: str


class AccountOut(BaseModel):
    id: str
    username: str
    email: str


class AccountOutWithHashedPassowrd(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


class Location(BaseModel):
    country: str
    state: str
    city: str


class TripIn(BaseModel):
    name: str
    type_of_trip: str
    max_attendees: str
    from_date: str
    to_date: str
    location: Location
    description: str
    image: str
    attendees: Optional[List[AccountOut]]


class TripOut(BaseModel):
    id: str
    name: str
    type_of_trip: str
    max_attendees: str
    from_date: str
    to_date: str
    location: Location
    description: str
    image: str
    account: AccountOut
    attendees: Optional[List[AccountOut]]


class TripList(BaseModel):
    trips: List[TripOut]
