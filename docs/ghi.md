# Customer Graphical Human Interface

## Home Page

This is the landing page for our site

![Home Page](wireframes/Home.png)

Logged in version

![Home Page Logged In](wireframes/Logged%20In%20Home.png)

## Trips

![Trips](wireframes/Trips.png)

## Create A Trip

![Create A Trip](wireframes/CreateTrip.png)
