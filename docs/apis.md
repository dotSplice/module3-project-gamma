# API

## ACCOUNTS

- **Method**: `POST`,
- **Path**: `/api/accounts`

Input:

```json
{
  "username": "string",
  "email": "string",
  "password": "string"
}
```

Output:

```json
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": "string",
    "username": "string",
    "email": "string"
  }
}
```

## TRIPS

- **Method**: `POST`, `GET`,
- **Path**: `/api/trips`

Input:

```json
{
  "name": "string",
  "type_of_trip": "string",
  "max_attendees": "string",
  "from_date": "string",
  "to_date": "string",
  "location": {
    "country": "string",
    "state": "string",
    "city": "string"
  },
  "description": "string",
  "image": "string",
  "attendees": [
    {
      "id": "string",
      "username": "string",
      "email": "string"
    }
  ]
}
```

Output

```json
{
  "id": "string",
  "name": "string",
  "type_of_trip": "string",
  "max_attendees": "string",
  "from_date": "string",
  "to_date": "string",
  "location": {
    "country": "string",
    "state": "string",
    "city": "string"
  },
  "description": "string",
  "image": "string",
  "account": {
    "id": "string",
    "username": "string",
    "email": "string"
  },
  "attendees": [
    {
      "id": "string",
      "username": "string",
      "email": "string"
    }
  ]
}
```

- **Method**: `PUT`
- **Path**: `/api/trips{id}`

Input

```json
{
  "id": "string",
  "name": "string",
  "type_of_trip": "string",
  "max_attendees": "string",
  "from_date": "string",
  "to_date": "string",
  "location": {
    "country": "string",
    "state": "string",
    "city": "string"
  },
  "description": "string",
  "image": "string",
  "account": {
    "id": "string",
    "username": "string",
    "email": "string"
  },
  "attendees": [
    {
      "id": "string",
      "username": "string",
      "email": "string"
    }
  ]
}
```

Output:

"string"

## MY TRIPS

- **Method**: `GET`,
- **Path**: `/api/mine`

Input:

Parameters

session_getter (query)
fastapi_token

Output:

```json
{
  "trips": [
    {
      "id": "string",
      "name": "string",
      "type_of_trip": "string",
      "max_attendees": "string",
      "from_date": "string",
      "to_date": "string",
      "location": {
        "country": "string",
        "state": "string",
        "city": "string"
      },
      "description": "string",
      "image": "string",
      "account": {
        "id": "string",
        "username": "string",
        "email": "string"
      },
      "attendees": [
        {
          "id": "string",
          "username": "string",
          "email": "string"
        }
      ]
    }
  ]
}
```

- **Method**: `DELETE`,
- **Path**: `/api/mine/{id}`

Input:

```json
{
  "id": "string"
}
```

Output:

"string"
