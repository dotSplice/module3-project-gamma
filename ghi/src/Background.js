import background4 from "./assets/background_movie4.mov";
import background5 from "./assets/background_movie5.mov";
import { useState, useEffect, useMemo } from "react";

const Background = () => {
  const backgrounds = useMemo(() => [background4, background5], []);
  const [index, setBackgroundIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setBackgroundIndex((prevIndex) => (prevIndex + 1) % backgrounds.length);
    }, 10000);

    return () => clearInterval(interval);
  }, [backgrounds]);

  useEffect(() => {
    const videoElement = document.getElementById("background-video");
    videoElement.src = backgrounds[index];
    videoElement.load();
    videoElement.play();
  }, [index, backgrounds]);

  return (
    <div>
      <video
        id="background-video"
        autoPlay
        loop
        muted
        style={{
          position: "fixed",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          zIndex: -1,
          objectFit: "cover",
        }}
      />
    </div>
  );
};

export default Background;
