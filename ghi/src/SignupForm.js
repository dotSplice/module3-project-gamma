import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { useSignupMutation } from "./app/authApiSlice";
import "./index.css";
import AlertError from "./AlertError";
import { Link } from "react-router-dom";

const SignupForm = () => {
  const [signup, signupResult] = useSignupMutation();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    if (signupResult.error) {
      setErrorMessage(signupResult.error.data.detail);
    }
    if (signupResult.isSuccess) navigate("/");
  }, [signupResult, navigate]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setErrorMessage("Passwords do not match");
      return;
    } else {
      signup({ username, email, password });
    }
  };
  return (
    <div className="d-flex justify-content-center">
      <div className="auth card text-bg-light mb-3">
        <h5 className="card-header">Sign Up</h5>
        <div className="card-body">
          <form onSubmit={(e) => handleSubmit(e)}>
            {errorMessage && <AlertError>{errorMessage}</AlertError>}
            <div className="mb-3">
              <label className="form-label">Username:</label>
              <input
                name="username"
                type="text"
                className="form-control"
                onChange={(e) => setUsername(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Email:</label>
              <input
                name="email"
                type="email"
                className="form-control"
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password:</label>
              <input
                name="password"
                type="password"
                className="form-control"
                onChange={(e) => {
                  setPassword(e.target.value);
                  setErrorMessage("");
                }}
                required
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Confirm Password:</label>
              <input
                name="confirm_password"
                type="password"
                className="form-control"
                onChange={(e) => {
                  setConfirmPassword(e.target.value);
                  setErrorMessage("");
                }}
                required
              />
            </div>
            <div>
              <input
                className="btn btn-primary"
                type="submit"
                value="Sign Up"
              />
            </div>
          </form>
          <div className="signup-footer mb-3">
            <p>
              Already have an account? <Link to="/login">Login</Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignupForm;
