import React, { useState, useEffect } from "react";
import "react-country-state-city/dist/react-country-state-city.css";
import { useCreateTripMutation } from "./app/tripsApiSlice";
import { useNavigate } from "react-router";
import { useGetAccountQuery } from "./app/authApiSlice";
import Footer from "./Footer";

function TripForm() {
  const [createTrip] = useCreateTripMutation();
  const [name, setName] = useState("");
  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");
  const [description, setDescription] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [typeOfTrip, setTypeOfTrip] = useState("");
  const [data, setData] = useState([]);
  const [getcountry, setCountry] = useState("");
  const [getstates, setStates] = useState([]);
  const [selectedState, setSelectedState] = useState();
  const [getcities, setCities] = useState([]);
  const [selectedCity, setSelectedCity] = useState();
  const [maxAttendees, setMaxAttendees] = useState("");
  const navigate = useNavigate();
  const [file, setFile] = useState();
  const { data: account } = useGetAccountQuery();
  const createAccount = () => {
    if (!account) {
      setErrorMessage("Please sign in or create an account");
    }
  };

  function handleChange(e) {
    console.log(e.target.value);
    setFile(e.target.value);
  }

  const getData = async () => {
    const url =
      "https://pkgstore.datahub.io/core/world-cities/world-cities_json/data/5b3dd46ad10990bca47b04b4739a02ba/world-cities_json.json";
    const data = await fetch(url);
    const pdata = await data.json();
    setData(pdata);
  };

  const country = [
    ...new Set(
      data.map((item) => {
        return item.country;
      })
    ),
  ];
  country.sort();

  useEffect(() => {
    getData();
  }, []);

  const tripTypes = [
    "Beach & Sea",
    "Classic Sights",
    "Culture & Arts",
    "Food & Drink",
    "Wellness",
    "Outdoors & Nature",
  ];

  const handleCountry = (event) => {
    setCountry(event.target.value);
    let states = data.filter((states) => {
      return states.country === event.target.value;
    });

    states = [
      ...new Set(
        states.map((item) => {
          return item.subcountry;
        })
      ),
    ];
    states.sort();
    setStates((getstates) => states);
  };
  const handleState = (e) => {
    setSelectedState(e.target.value);
    let cities = data.filter((city) => {
      return city.subcountry === e.target.value;
    });
    cities = [
      ...new Set(
        cities.map((item) => {
          return item.name;
        })
      ),
    ];

    cities.sort();
    setCities((getcities) => cities);
  };

  const handleCity = (e) => {
    setSelectedCity(e.target.value);
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };
  const handleFromDateChange = (event) => {
    const value = event.target.value;
    setFromDate(value);
  };
  const handleToDateChange = (event) => {
    const value = event.target.value;
    setToDate(value);
  };
  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  };
  const handleTripTypeChange = (event) => {
    const value = event.target.value;
    setTypeOfTrip(value);
  };

  const handleMaxAttendeesChange = (event) => {
    setMaxAttendees(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name: name,
      max_attendees: maxAttendees,
      type_of_trip: typeOfTrip,
      from_date: fromDate,
      to_date: toDate,
      location: {
        country: getcountry,
        state: selectedState,
        city: selectedCity,
      },
      description: description,
      image: file,
    };
    createTrip(data);
    if (!account) {
      navigate("/trips/create");
    } else {
      navigate("/trips");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a trip</h1>
          <div className="text-danger">{errorMessage}</div>
          <form onSubmit={handleSubmit} id="create-trip-form">
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Trip Name
              </label>
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Type
              </label>
              <select
                value={typeOfTrip}
                onChange={handleTripTypeChange}
                required
                id="type_of_trip"
                name="type_of_trip"
                className="form-select"
              >
                <option>Choose type</option>
                {tripTypes.map((trip, index) => {
                  return (
                    <option key={index} value={trip}>
                      {trip}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="max_attendees" className="form-label">
                Max Attendees
              </label>
              <input
                value={maxAttendees}
                onChange={handleMaxAttendeesChange}
                placeholder="Max Attendees"
                required
                type="number"
                name="max_attendees"
                id="max_attendees"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="from_date" className="form-label">
                From
              </label>
              <input
                value={fromDate}
                onChange={handleFromDateChange}
                placeholder="From"
                required
                type="date"
                name="from_date"
                id="from_date"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="to_date" className="form-label">
                To
              </label>
              <input
                value={toDate}
                onChange={handleToDateChange}
                placeholder="To"
                required
                type="date"
                name="to_date"
                id="to_date"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Country
              </label>
              <select
                className="form-select"
                value={getcountry}
                onChange={(event) => handleCountry(event)}
              >
                <option>Select country ...</option>
                {country.map((item, index) => {
                  return (
                    <option value={item} key={item}>
                      {item}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                State
              </label>
              <select
                className="form-select"
                value={selectedState}
                onChange={(e) => handleState(e)}
                required
              >
                <option>Select State ...</option>
                {getstates.map((item, index) => {
                  return (
                    <option value={item} key={item}>
                      {item}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                City
              </label>
              <select
                className="form-select"
                value={selectedCity}
                onChange={(e) => handleCity(e)}
                required
              >
                <option>Select City ...</option>
                {getcities.map((item, index) => {
                  return (
                    <option key={index} value={item}>
                      {item}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">
                Description
              </label>
              <textarea
                value={description}
                onChange={handleDescriptionChange}
                className="form-control"
                id="description"
                name="description"
                rows="3"
                required
              ></textarea>
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">
                Image
              </label>
              <input
                className="form-control"
                placeholder="Image url..."
                type="url"
                onChange={handleChange}
                required
              />
            </div>
            {file && (
              <div className="mb-3">
                <img
                  style={{ width: "auto", height: "200px" }}
                  src={file}
                  alt="file"
                />
              </div>
            )}
            <button className="btn btn-primary" onClick={() => createAccount()}>
              Create
            </button>
          </form>
        </div>
      </div>
      <div>
        <br />
        <br />
        <br />
        <Footer />
      </div>
    </div>
  );
}

export default TripForm;
