import { NavLink } from "react-router-dom";
export default function Footer() {
  return (
    <footer className="footer">
      <p className="footer-text">
        <span>Copyright Trip© 2023</span>
        <span>
          <NavLink to="/privacy">Privacy & Legal</NavLink>
        </span>
        <span>
          {" "}
          <NavLink to="/contact-us">Contact Us</NavLink>
        </span>
        <span>
          {" "}
          <NavLink to="/about">About</NavLink>
        </span>
      </p>
    </footer>
  );
}
