import "./App.css";
// import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
// import { BrowserRouter, Routes, Route } from "react-router-dom";
// import LoginForm from "./LoginForm.js";
// import SignupForm from "./SignupForm.js";
// import Home from "./Home";

import Nav from "./Nav";
import { Outlet } from "react-router-dom";

function App() {
  return (
    <>
      <div>
        <Nav />
        <div className="mt-5">
          <div className="container">
            <Outlet />
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
