import React, { useState } from "react";
import { useGetAccountQuery } from "./app/authApiSlice";
import { useGetAllTripsQuery } from "./app/tripsApiSlice";
import { useNavigate } from "react-router-dom";
import Footer from "./Footer";

export default function TripsThatIJoined() {
  const navigate = useNavigate();
  const { data: account } = useGetAccountQuery();
  const { data: tripsData, isLoading, error } = useGetAllTripsQuery();
  const [filterValue, setFilterValue] = useState("");

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error.message}</div>;
  if (!tripsData) return null;

  const joinedTrips = tripsData.trips.filter((trip) => {
    return (
      trip.attendees &&
      trip.attendees.some((attendee) => attendee.id === account.id)
    );
  });

  const viewTripDetails = (tripId) => {
    navigate(`/trips/view/${tripId}`);
  };
  const handleFilterChange = (event) => {
    setFilterValue(event.target.value);
  };
  const filteredTrips = joinedTrips.filter((trip) => {
    return trip.name.toLowerCase().includes(filterValue.toLowerCase());
  });

  return (
    <>
      <center>
        <h1>Joined Trips</h1>
      </center>
      <div className="searchbar">
        <input
          className="form-control"
          type="text"
          placeholder="Search trips by name"
          value={filterValue}
          onChange={handleFilterChange}
        />
      </div>
      <div className="row">
        {filteredTrips.map((trip) => (
          <div key={trip.id} className="col-lg-4 d-flex justify-content-center">
            <div className="card" style={{ width: "18rem" }}>
              <img src={trip.image} className="card-img-top" alt={trip.name} />
              <div className="card-body">
                <h5 className="card-title">{trip.name}</h5>
                <div className="card-text">
                  From: {trip.from_date}
                  <br />
                  To: {trip.to_date}
                  <br />
                  Max Attendees: {trip.max_attendees}
                  <br />
                  {!trip.attendees ? (
                    <p>Attendees: 0</p>
                  ) : (
                    <p>Attendees: {trip.attendees.length}</p>
                  )}
                  <br />
                  <br />
                  Description: <br />
                  {trip.description}
                </div>
              </div>
              <div className="btn-group">
                <button
                  className="btn btn-primary"
                  onClick={() => viewTripDetails(trip.id)}
                >
                  View Details
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div>
        <br />
        <br />
        <br />
        <Footer />
      </div>
    </>
  );
}
