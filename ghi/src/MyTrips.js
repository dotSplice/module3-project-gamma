import React, { useState } from "react";
import { useGetMyTripsQuery } from "./app/tripsApiSlice";
import { useDeleteMutation } from "./app/tripsApiSlice";
import { useNavigate } from "react-router-dom";
import Footer from "./Footer";

export default function MyTrips() {
  const navigate = useNavigate();
  const { data, error, isLoading } = useGetMyTripsQuery();
  const [filterValue, setFilterValue] = useState("");
  const [deleteTrip] = useDeleteMutation();
  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error.message}</div>;
  if (!data) return null;

  const handleDelete = (id) => {
    deleteTrip(id);
  };
  const handleFilterChange = (event) => {
    setFilterValue(event.target.value);
  };
  const filteredTrips = data.trips.filter((trip) => {
    return trip.name.toLowerCase().includes(filterValue.toLowerCase());
  });
  return (
    <>
      <center>
        <h1>My Trips</h1>
      </center>
      <div className="row">
        <div className="searchbar">
          <input
            className=" form-control"
            type="text"
            placeholder="Search trips by name"
            value={filterValue}
            onChange={handleFilterChange}
          />
        </div>
        {filteredTrips.map((trip) => {
          return (
            <div
              key={trip.id}
              className="col-lg-4 d-flex justify-content-center"
            >
              <div className="card" style={{ width: "18rem" }}>
                <img
                  src={trip.image}
                  className="card-img-top"
                  alt={trip.name}
                />
                <div className="card-body">
                  <h5 className="card-title">{trip.name}</h5>
                  <div className="card-text">
                    From: {trip.from_date}
                    <br />
                    To: {trip.to_date}
                    <br />
                    Max Attendees: {trip.max_attendees}
                    <br />
                    {!trip.attendees ? (
                      <p>Attendees: 0</p>
                    ) : (
                      <p> Attendees: {trip.attendees.length}</p>
                    )}
                    <br />
                    <br />
                    Description: <br />
                    {trip.description}
                  </div>
                </div>
                <div className="btn-group">
                  <>
                    <button
                      className="btn btn-primary "
                      onClick={() => navigate(`/trips/view/${trip.id}`)}
                    >
                      View Details
                    </button>
                    <button
                      onClick={() => handleDelete(trip.id)}
                      className="btn btn-primary btn-danger"
                    >
                      Delete
                    </button>

                    <button
                      className="btn btn-primary btn-warning"
                      onClick={() => navigate(`/trips/${trip.id}`)}
                    >
                      Edit
                    </button>
                  </>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div>
        <br />
        <br />
        <br />
        <Footer />
      </div>
    </>
  );
}
