import React, { useState } from "react";
import { useGetAllTripsQuery } from "./app/tripsApiSlice";
import { useGetAccountQuery } from "./app/authApiSlice";
import { useUpdateMutation } from "./app/tripsApiSlice";
import { useNavigate } from "react-router-dom";
import Footer from "./Footer";
export default function Trips() {
  const navigate = useNavigate();
  const { data, error, isLoading } = useGetAllTripsQuery();
  const { data: account } = useGetAccountQuery();
  const [updateTrip] = useUpdateMutation();

  const [filterValue, setFilterValue] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("All");

  const tripCategories = [
    "All",
    "Beach & Sea",
    "Classic Sights",
    "Culture & Arts",
    "Food & Drink",
    "Wellness",
    "Outdoors & Nature",
  ];

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error.message}</div>;
  if (!data) return null;

  const handleFilterChange = (event) => {
    setFilterValue(event.target.value);
  };

  let tripsToDisplay = data.trips.filter((trip) => {
    return trip.name.toLowerCase().includes(filterValue.toLowerCase());
  });

  if (selectedCategory !== "All") {
    tripsToDisplay = tripsToDisplay.filter(
      (trip) => trip.type_of_trip === selectedCategory
    );
  }

  const joinTrip = (trip, account) => {
    if (!account) {
      alert("Please log in to join a trip!");
      navigate("/login");
      return;
    }
    const data = { ...trip };
    if (!data.attendees || data.attendees.length === 0) {
      data.attendees = [account];
    } else {
      for (let attendee of data.attendees) {
        if (attendee.id === account.id) {
          console.log("includes account");
          return;
        }
      }
      data.attendees = [...data.attendees, account];
    }

    console.log(data);
    updateTrip(data);
  };

  return (
    <>
      <center>
        <h1>Trips</h1>
      </center>

      <div className="d-flex justify-content-center mb-3">
        <div className="btn-group" role="group" aria-label="Category selection">
          {tripCategories.map((type_of_trip, index) => (
            <button
              key={type_of_trip}
              className={`btn ${
                selectedCategory === type_of_trip
                  ? "btn-primary"
                  : "btn-outline-primary"
              } ${index !== 0 ? "ml-4" : ""}`}
              onClick={() => setSelectedCategory(type_of_trip)}
            >
              {type_of_trip}
            </button>
          ))}
        </div>
      </div>

      <div className="row">
        <div className="searchbar">
          <input
            className=" form-control"
            type="text"
            placeholder="Search trips by name"
            value={filterValue}
            onChange={handleFilterChange}
          />
        </div>

        {tripsToDisplay.map((trip) => {
          return (
            <div
              key={trip.id}
              className="col-lg-4 d-flex justify-content-center"
            >
              <div className="card" style={{ width: "18rem" }}>
                <img
                  src={trip.image}
                  className="card-img-top"
                  alt={trip.name}
                />
                <div className="card-body">
                  <h5 className="card-title">{trip.name}</h5>
                  <div className="card-text">
                    Max Attendees: {trip.max_attendees}
                    <br />
                    {!trip.attendees ? (
                      <p>Attendees: 0</p>
                    ) : (
                      <p> Attendees: {trip.attendees.length}</p>
                    )}
                    <br />
                    {trip.description}
                  </div>
                </div>
                <div className="btn-group ">
                  {trip.attendees.length.toString() === trip.max_attendees ? (
                    <button className="btn btn-primary disabled">Full</button>
                  ) : (
                    <>
                      <button
                        className="btn btn-primary "
                        onClick={() => navigate(`/trips/view/${trip.id}`)}
                      >
                        View Details
                      </button>
                      <button
                        onClick={() => joinTrip(trip, account)}
                        className="btn btn-primary btn-info "
                      >
                        Join
                      </button>
                    </>
                  )}
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div>
        <br />
        <br />
        <br />
        <Footer />
      </div>
    </>
  );
}
