import meme from "./assets/giphy.webp";
import Nav from "./Nav";

const ErrorPage = () => {
  return (
    <div className="container">
      <Nav />
      <div className="mt-5">
        <h1>Page not found</h1>
        <img src={meme} alt="error meme" />
      </div>
    </div>
  );
};

export default ErrorPage;
